<?php
if (isset($_GET['tournoi'])) {
    foreach($tabTournois as $tournoi) 
    {
        if($tournoi->id_t == $_GET['tournoi'])
        {
        ?>

            <div>
                <div>
                    <h2><?= $tournoi->nom_t ?></h2>
                    <h3><?= $tournoi->date_debut ?></h3>
                </div>
                <div>
                    <h4>Participants :</h4>
                    <ul>
                    <?php
                    foreach ($tournoi->joueurs as $joueur) {
                        ?>
                        <li><?= $joueur->nom_j ?> <?= $joueur->prenom_j ?></li>
                        <?php
                    }
                    ?>
                    </ul>
                </div>
                <div>
                <ul>
                    <?php
                    foreach ($tournoi->tours as $tour) {
                        ?>
                        <li>
                        <article>
                            <span>
                                <em><?= $tour->nom_tour ?></em>
                                <p><?= count($tour->matchs) ?> matchs</p>
                            </span>
                            <ul>
                        <?php
                        foreach ($tour->matchs as $match) {
                            ?>
                                <li> <?= $match->joueur1->nom_j ?> <em>VS</em> <?= $match->joueur2->nom_j ?> </li>
                            <?php
                        }
                        ?>
                        </ul>
                        </article>
                        <?php
                    }
                    ?>
                    </li>
                    </ul>
                </div>
            </div>

        <?php
        }
    }
}
?>