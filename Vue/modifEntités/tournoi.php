<?php
if (isset($_GET['tournoi'])) {
    foreach ($tabTournois as $tournoi) {
        if ($tournoi->id_t == $_GET['tournoi']) {
?>

            <div>
                <div>
                    <h2><?= $tournoi->nom_t ?></h2>
                    <h3><?= $tournoi->date_debut ?></h3>
                </div>
                <div>
                    <h4>Modification des attributs :</h4>
                    <form action="index.php" method="post">
                        <label>Identifiant : <input type="number" name="id_t" value="<?= $tournoi->id_t ?>" hidden></label>
                        <label>Nom : <input type="text" name="nom_t" value="<?= $tournoi->nom_t ?>"></label>
                        <label>Date de début : <input type="date" name="date_debut" value="<?= $tournoi->date_debut ?>"></label>
                        <label>Date de fin : <input type="date" name="date_fin" value="<?= $tournoi->date_fin ?>"></label>
                        <button type="submit" name="btnModifT">Validé</button>
                    </form>
                    <div>
                        <h4>Participant(s) :</h4>
                        <ul>
                            <?php
                            foreach ($tournoi->joueurs as $joueur) {
                            ?>
                                <li><?= $joueur->nom_j ?> <?= $joueur->prenom_j ?> <button onclick="window.location.href = '../Controller/index.php?tournoi=<?= $tournoi->id_t ?>&suppJT=<?= $joueur->id_j ?>';">Supprimé</button></li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <button onclick="window.location.href = '../Controller/index.php?ajoutJoueurT=0&t=<?= $tournoi->id_t ?>';">Ajouter un joueur au tournoi</button>
                </div>
                <div>
                    <ul>
                        <?php
                        foreach ($tournoi->tours as $tour) {
                        ?>
                            <li>
                                <article>
                                    <span>
                                        <em><?= $tour->nom_tour ?></em>
                                        <p><?= count($tour->matchs) ?> matchs</p>
                                    </span>
                                    <ul>
                                        <?php
                                        foreach ($tour->matchs as $match) {
                                        ?>
                                            <li> <?= $match->joueur1->nom_j ?> <em>VS</em> <?= $match->joueur2->nom_j ?> </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </article>
                            <?php
                        }
                            ?>
                            </li>
                    </ul>
                </div>
            </div>

<?php
        }
    }
}
?>