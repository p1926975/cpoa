<?php
if (isset($_GET['joueur'])) {
    foreach($tabJoueurs as $joueur) 
    {
        if($joueur->id_j == $_GET['joueur'])
        {
        ?>

            <div>
                <div>
                    <img src="<?= $joueur->image ?>" alt="Photo de <?= $joueur->nom_j?> <?= $joueur->prenom_j ?>">
                    <div>
                        <h2><?= $joueur->nom_j?> <?= $joueur->prenom_j ?></h2>
                        <p>Age : <?= $joueur->getAge() ?></p>
                    </div>
                </div>
                <p>Nombre de match(s) joué(s) : <?= $joueur->nbMatch_j ?></p>
                <p>Nombre de match(s) gagné(s) : <?= $joueur->nbVictoires ?></p>
                <p>Pourcentage de victoire : <?= $joueur->getWinRate() ?></p>
            </div>


        <?php
        }
    }
}
?>