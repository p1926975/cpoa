<h1>Liste de joueurs</h1>

<div>
    <?php
    // ajouter requete dans le index 
    foreach ($tabJoueurs as $joueur)    
    {
        echo "<article>";
        echo "<div>";
        echo "<img src=\"" . $joueur->image . "\" alt=\"Photo de " . $joueur->nom_j . " " . $joueur->prenom_j . "\">";
        echo "<p><a href='../Controller/index?joueur=".$joueur->id_j."'>Le joueur ". $joueur->nom_j . " " . $joueur->prenom_j ."</a></p>";
        echo "<div>";
        echo "<p>Age : " . $joueur->getAge() . " ans</p>";
        echo "<p>Pourcentage de réussite : " . $joueur->getWinRate() . "</p>";
        echo "</div>";
        echo "</div>";
        echo "</article>";
    }

    ?>
</div>
