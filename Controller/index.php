<?php

session_start();
$head = "head";
$page = "";
$titre = "defaut";
$buttonCo = "connection";
$pageAdmin = ["acceuilAdmin", ""];

require_once('../modele/DAO_Joueur.php');
require_once('../modele/DAO_Matchs.php');
require_once('../modele/DAO_Tour.php');
require_once('../modele/DAO_Tournoi.php');

require_once('../modele/DTO_Joueur.php');
require_once('../modele/DTO_Matchs.php');
require_once('../modele/DTO_Tour.php');
require_once('../modele/DTO_Tournoi.php');

// GESTION DES OBJs
// chargement des joueurs
$DAOJoueur = new DAO_Joueur();
$tabJoueurs = $DAOJoueur->getAll();

// chargement des tournois
$DAOTournois = new DAO_Tournoi();
$tabTournois = $DAOTournois->getAll();

// chargement des tours
$DAOTours = new DAO_Tour();
$tabTours = $DAOTours->getAll();

// chargement des matchs
$DAOMatchs = new DAO_Matchs();
$tabMatchs = $DAOMatchs->getAll();

// ATTRIBUTION DES LISTES DANS LES OBJs
$tabTournois = $DAOTournois->chargementTab($tabJoueurs, $tabTours, $tabTournois);
$tabTours = $DAOTours->chargementTab($tabMatchs, $tabTours);
$tabMatchs = $DAOMatchs->chargementOBJ($tabJoueurs, $tabMatchs);

// GESTION DE LA CONNEXION
if (isset($_POST['btnConnexion'])) {
    if ($_POST['username'] == "admin" && $_POST['userpass'] == "Admin") {
        $_SESSION['connected'] = true;
        $_SESSION['name'] = $_POST['username'];
        $page = "acceuilAdmin";
        $titre = "acceuilAdmin";
        $buttonCo = "deconnection";
        $head = "headAdmin";
    } else {
        echo "erreur";
    }
}
//formulaire de creation du joueur
elseif (isset($_POST['btnCreationJ'])) {
    if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['nationalité']) && isset($_POST['date'])) {
        if (isset($_POST['photo'])) {
            $DAOJoueur->insert($_POST['nom'], $_POST['prenom'], $_POST['nationalité'], $_POST['date'], $_POST['photo']);
        } else {
            $DAOJoueur->insert($_POST['nom'], $_POST['prenom'], $_POST['nationalité'], $_POST['date']);
        }
        $page = "acceuilAdmin";
        $titre = "acceuilAdmin";
        $head = "headAdmin";
    }
}
//formulaire création tournoi
elseif (isset($_POST['btnCreationT'])) {
    if (isset($_POST['nom']) && isset($_POST['date-d']) && isset($_POST['date-f'])) {
        $DAOTournois->insert($_POST['nom'], $_POST['date-d'], $_POST['date-f'], -1);
        $page = "acceuilAdmin";
        $titre = "acceuilAdmin";
        $head = "headAdmin";
    }
}
//formulaire modification tournoi
elseif (isset($_POST['btnModifT'])) {
    $DAOTournois->update($_POST['id_t'], $_POST['nom_t'], $_POST['date_debut'], $_POST['date_fin']);
}
//formulaire ajout joueur au tournoi
elseif (isset($_POST['btnajoutJT'])) {
    $DAOTournois->ajoutJoueur($_POST['ajoutJT'], $_POST['tournoi']);
}
//formulaire suppression joueur dans tournoi
elseif (isset($_GET['suppJT'])) {
    $DAOTournois->supprimeJoueur($_GET['suppJT'], $_GET['tournoi']);
}


if (isset($_GET['Deconnexion'])) {
    unset($_SESSION['connected']);
}

// AFFECTATION DES PAGES
if ($page == "") {
    if (isset($_GET['ajoutJoueurT'])) {
        $page = "modifEntités/ajoutJoueurT";
        $titre = "Nouveau joueur tournoi";
    } elseif (isset($_GET['tournoi'])) {
        if (isset($_SESSION['connected'])) {
            $page = "modifEntités/tournoi";
            $titre = "tournoi";
        } else {
            $page = "detailsEntités/tournoi";
            $titre = "tournoi";
        }
    } elseif (isset($_GET['joueur'])) {
        $page = "detailsEntités/joueur";
        $titre = "joueur";
    } elseif (isset($_GET['page'])) {
        if ($_GET['page'] == "acceuilAdmin" && !isset($_SESSION['connected'])) {
            $page = "acceuilJoueur";
            $titre = "acceuilJoueur";
            $head = "head";
        } else {
            $page = $_GET['page'];
            $titre = $_GET['page'];
        }
    } else {
        $page = "acceuilJoueur";
        $titre = "acceuilJoueur";
    }
}

if (isset($_SESSION['connected'])) {
    $head = "headAdmin";
}



require_once('../Vue/header.php');
require_once('../Vue/' . $head . '.php');
require_once('../Vue/navigation.php');
require_once('../Vue/' . $page . '.php');
require_once('../Vue/footer.html');
