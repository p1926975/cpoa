-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 16 juin 2021 à 07:27
-- Version du serveur :  8.0.21
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestiontennis`
--

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--

DROP TABLE IF EXISTS `joueur`;
CREATE TABLE IF NOT EXISTS `joueur` (
  `id_j` int NOT NULL AUTO_INCREMENT,
  `nom_j` varchar(20) NOT NULL,
  `prenom_j` varchar(20) NOT NULL,
  `nationalite` varchar(20) NOT NULL,
  `date_naissance` date NOT NULL,
  `nbMatch_j` int NOT NULL,
  `nbVictoires` int NOT NULL,
  `image` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`id_j`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `joueur_tournoi`
--

DROP TABLE IF EXISTS `joueur_tournoi`;
CREATE TABLE IF NOT EXISTS `joueur_tournoi` (
  `id_j` int NOT NULL,
  `id_t` int NOT NULL,
  KEY `FK_JT_j` (`id_j`),
  KEY `FK_JT_t` (`id_t`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `matchs`
--

DROP TABLE IF EXISTS `matchs`;
CREATE TABLE IF NOT EXISTS `matchs` (
  `id_m` int NOT NULL AUTO_INCREMENT,
  `joueur1` int NOT NULL,
  `joueur2` int NOT NULL,
  `resultatJ1` int NOT NULL,
  `resultatJ2` int NOT NULL,
  `date` time NOT NULL,
  PRIMARY KEY (`id_m`),
  KEY `FK_MATCH_j1` (`joueur1`),
  KEY `FK_MATCH_j2` (`joueur2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `match_tour`
--

DROP TABLE IF EXISTS `match_tour`;
CREATE TABLE IF NOT EXISTS `match_tour` (
  `id_m` int NOT NULL,
  `id_t` int NOT NULL,
  KEY `FK_MT_m` (`id_m`),
  KEY `FK_MT_t` (`id_t`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `resultat_match`
--

DROP TABLE IF EXISTS `resultat_match`;
CREATE TABLE IF NOT EXISTS `resultat_match` (
  `id_m` int NOT NULL,
  `res1` varchar(20) NOT NULL,
  `res2` varchar(20) NOT NULL,
  KEY `FK_RM_m` (`id_m`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tour`
--

DROP TABLE IF EXISTS `tour`;
CREATE TABLE IF NOT EXISTS `tour` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom_tour` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tournoi`
--

DROP TABLE IF EXISTS `tournoi`;
CREATE TABLE IF NOT EXISTS `tournoi` (
  `id_t` int NOT NULL AUTO_INCREMENT,
  `nom_t` varchar(30) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `nbMatch` int NOT NULL,
  PRIMARY KEY (`id_t`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tour_tournoi`
--

DROP TABLE IF EXISTS `tour_tournoi`;
CREATE TABLE IF NOT EXISTS `tour_tournoi` (
  `id` int NOT NULL,
  `id_t` int NOT NULL,
  KEY `FK_TT_tour` (`id`),
  KEY `FK_TT_tournoi` (`id_t`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `joueur_tournoi`
--
ALTER TABLE `joueur_tournoi`
  ADD CONSTRAINT `FK_JT_j` FOREIGN KEY (`id_j`) REFERENCES `joueur` (`id_j`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `FK_JT_t` FOREIGN KEY (`id_t`) REFERENCES `tournoi` (`id_t`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `matchs`
--
ALTER TABLE `matchs`
  ADD CONSTRAINT `FK_MATCH_j1` FOREIGN KEY (`joueur1`) REFERENCES `joueur` (`id_j`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `FK_MATCH_j2` FOREIGN KEY (`joueur2`) REFERENCES `joueur` (`id_j`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `match_tour`
--
ALTER TABLE `match_tour`
  ADD CONSTRAINT `FK_MT_m` FOREIGN KEY (`id_m`) REFERENCES `matchs` (`id_m`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `FK_MT_t` FOREIGN KEY (`id_t`) REFERENCES `tour` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `resultat_match`
--
ALTER TABLE `resultat_match`
  ADD CONSTRAINT `FK_RM_m` FOREIGN KEY (`id_m`) REFERENCES `matchs` (`id_m`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `tour_tournoi`
--
ALTER TABLE `tour_tournoi`
  ADD CONSTRAINT `FK_TT_tour` FOREIGN KEY (`id`) REFERENCES `tour` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `FK_TT_tournoi` FOREIGN KEY (`id_t`) REFERENCES `tournoi` (`id_t`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
