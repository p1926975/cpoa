-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 16 juin 2021 à 07:28
-- Version du serveur :  8.0.21
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestiontennis`
--

--
-- Déchargement des données de la table `joueur`
--

INSERT INTO `joueur` (`id_j`, `nom_j`, `prenom_j`, `nationalite`, `date_naissance`, `nbMatch_j`, `nbVictoires`, `image`) VALUES
(1, 'TEST', 'test', 'Test', '2020-01-01', 10, 5, '/null'),
(2, 'ronald', 'thièry', 'pigeon', '1999-12-30', 500, 2, '/null'),
(7, 'Reeeeee', 'REEEEEEEEEE', 'RERERERE', '1000-03-15', 0, 0, '/null');

--
-- Déchargement des données de la table `joueur_tournoi`
--

INSERT INTO `joueur_tournoi` (`id_j`, `id_t`) VALUES
(1, 1),
(2, 1);

--
-- Déchargement des données de la table `matchs`
--

INSERT INTO `matchs` (`id_m`, `joueur1`, `joueur2`, `resultatJ1`, `resultatJ2`, `date`) VALUES
(1, 1, 2, 1, 0, '15:26:10'),
(2, 1, 2, 1, 0, '15:26:10'),
(3, 1, 2, 1, 0, '15:26:10'),
(4, 2, 1, 1, 0, '15:26:10'),
(5, 2, 1, 0, 1, '15:26:10'),
(6, 2, 1, 1, 0, '15:26:10');

--
-- Déchargement des données de la table `match_tour`
--

INSERT INTO `match_tour` (`id_m`, `id_t`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(5, 3),
(6, 3);

--
-- Déchargement des données de la table `resultat_match`
--

INSERT INTO `resultat_match` (`id_m`, `res1`, `res2`) VALUES
(1, '999', '0'),
(2, '999', '0'),
(3, '000', '999'),
(4, '0', '999'),
(5, '0', '999'),
(6, '999', '0');

--
-- Déchargement des données de la table `tour`
--

INSERT INTO `tour` (`id`, `nom_tour`) VALUES
(1, 'cart-de-final'),
(2, 'demi-final'),
(3, 'final');

--
-- Déchargement des données de la table `tournoi`
--

INSERT INTO `tournoi` (`id_t`, `nom_t`, `date_debut`, `date_fin`, `nbMatch`) VALUES
(1, 'Tournoi de Tassin', '2010-12-12', '2021-12-12', 30),
(2, 'Les Bleues', '2000-06-18', '2001-06-18', 0);

--
-- Déchargement des données de la table `tour_tournoi`
--

INSERT INTO `tour_tournoi` (`id`, `id_t`) VALUES
(1, 1),
(2, 1),
(3, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
