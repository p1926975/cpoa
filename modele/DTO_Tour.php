<?php

class Tour {

    private $id;
    private $nom_tour;
    private $matchs;

    public function __construct($i=0, $nom)
    {
        $this->id = $i;
		$this->nom_tour = $nom;
    }

    public function __get($input) 
    {
        switch($input) 
        {
            case 'id':
                return $this->id;
                break;
            case 'nom_tour':
                return $this->nom_tour;
                break;
            case 'matchs' :
                return $this->matchs;
                break;
        }
    }

    public function __set($input, $value)
     {
        switch($input) 
        {
            case 'id':
                $this->id= $value;
                break;
            case 'nom_tour':
                $this->nom_tour = $value;
                break;
            case 'matchs' :
                $this->matchs = $value;
                break;
        }
    }
}
