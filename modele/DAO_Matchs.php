<?php
require_once('DTO_Matchs.php');

class DAO_Matchs {

	private $bdd;

	public function __construct()
	{
		try {
			$this->bdd = new PDO('mysql:host=127.0.0.1;dbname=gestionTennis;charset=UTF8', 'root', '');
		} catch(Exception $e) {
			die('Erreur: '.$e->getMessage());
		}
	}

    public function getById($i)
	{
		$sql = 'SELECT * FROM Matchs WHERE id_m=?';
		$req = $this->bdd->prepare($sql);
		$rep = $req->execute([$i]);
		$data = $req->fetch();
		$matchs = new Matchs(['id_m'],['joueur1'],['joueur2'],['resultatJ1'],['resultatJ2']);
		return $matchs;
	}

    public function getAll()
	{
		$tab = [];
		$sql = "SELECT * FROM Matchs";
		$articles = $this->bdd->query($sql);
		foreach ($articles as $data) {
			$tab[] = new matchs((int)$data['id_m'], (int)$data['joueur1'], (int)$data['joueur2'], (int)$data['resultatJ1'], (int)$data['resultatJ2']);
		}
		return $tab;
	}

	/*public function getGagnant($idM)
	{
		//Dans le cas ou c'est J1 qui a gagné
		$sql = 'SELECT id_j FROM Joueur INNER JOIN Matchs m ON j.id_j = m.joueur1 WHERE id_m = ? and resultatJ1=1';
		$req = $this->bdd->prepare($sql);
		$rep = $req->execute([$idM]);
		$data = $req->fetch();
		$reqJ1 = $data['id_j'];
		//Dans le cas ou c'est J2 qui a gagné
		$sql = 'SELECT id_j FROM Joueur INNER JOIN Matchs m ON j.id_j = m.joueur2 WHERE id_m = ? and resultatJ2=1))';
		$req = $this->bdd->prepare($sql);
		$rep = $req->execute([$idM]);
		$data = $req->fetch();
		$reqJ2 = $data['id_j'];
		//Ici on récupére l'identifiant du gagnant
		$sql = 'SELECT id_j FROM Joueur WHERE id_j IN (?, ?)';
		$req = $this->bdd->prepare($sql);
		$rep = $req->execute(array($reqJ1, $reqJ1));
		$data = $req->fetch();
		$req = $data['id_j'];
	}*/

	/*public function setResultat($idMatch, $res1, $res2)
	{
		$sql = 'UPDATE TABLE Match SET resultatJ1=? , resultatJ2=? WHERE id_m=?';
		$req = $this->bdd->prepare($sql);
		$req->execute(array($res1, $res2, $idMatch));
	}*/

	public function insert($j1, $j2, $rJ1, $rJ2)
	{		
		$sql = "INSERT INTO article (joueur1, joueur2, resultatJ1, resultatJ2) VALUES (?, ?, ?, ?)";
		$req = $this->bdd->prepare($sql);
		$req->execute(array($j1, $j2, $rJ1, $rJ2));
	}

	public function chargementOBJ($joueurs, $matchs)
	{
		$tabM = [];

		foreach ($matchs as $match) {
			foreach ($joueurs as $joueur) {
				if ($joueur->id_j == $match->joueur1) {
					$match->joueur1 = $joueur;
				}
			}
			foreach ($joueurs as $joueur) {
				if ($joueur->id_j == $match->joueur2) {
					$match->joueur2 = $joueur;
				}
			}
			$tabM[] = $match;
		}
		return $tabM;
	}
}