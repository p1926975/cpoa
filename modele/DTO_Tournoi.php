<?php
 
 class Tournoi{
     private $id_t;
     private $nom_t;
     private $date_debut;
     private $date_fin;
     private $joueurs;
     private $nbMatch;
     private $tours;

     public function __construct($i=0, $n, $dD, $dF, $nbM)
     {
         $this->id_t = $i;
         $this->nom_t = $n;
         $this->date_debut = $dD;
         $this->date_fin = $dF;
         $this->nbMatch = $nbM;
     }
     public function __get($name)
     {
         switch($name) {
             case 'id_t' :
                return $this->id_t;
                break;
             case 'nom_t' :
                return $this->nom_t;
                break;
             case 'date_debut' :
                return $this->date_debut;
                break;
             case 'date_fin' :
                return $this->date_fin;
                break;
             case 'joueurs' :
                return $this->joueurs;
                break;
             case 'nbMatch' :
                return $this->nbMatch;
                break;
             case 'tours' :
                return $this->tours;
                break;
         }
     }
     public function __set($name, $value)
     {
         switch($name) {
             case 'nom_t' :
                $this->nom_t = $value;
                break;
             case 'date_debut' :
                $this->date_debut = $value;
                break;
             case 'date_fin' :
                $this->date_debut = $value;
                break;
             case 'joueurs' :
                $this->joueurs = $value;
                break;
             case 'nbMatch' :
                $this->nbMatch = $value;
                break;
             case 'tours' :
                $this->tours = $value;
                break;
         }
     }
 }