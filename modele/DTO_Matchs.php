<?php

class Matchs {

    private $id_m;
    private $joueur1;
    private $joueur2;
    private $resultatJ1;
    private $resultatJ2;

    public function __construct($i=0, $j1, $j2, $rJ1, $rJ2)
    {
        $this->id_m = $i;
		$this->joueur1 = $j1;
		$this->joueur2 = $j2;
        $this->resultatJ1 = $rJ1;
		$this->resultatJ2 = $rJ2;
    }

    public function __get($input) 
    {
        switch($input) 
        {
            case 'id_m':
                return $this->id_m;
                break;
            case 'joueur1':
                return $this->joueur1;
                break;
            case 'joueur2':
                return $this->joueur2;
                break;
            case 'resultatJ1':
                return $this->resultatJ1;
                break;
            case 'resultatJ2':
                return $this->resultatJ2;
                break;
        }
    }

    public function __set($input, $value)
     {
        switch($input) 
        {
            case 'id_m':
                $this->id_m = $value;
                break;
            case 'joueur1':
                $this->joueur1 = $value;
                break;
            case 'joueur2':
                $this->joueur2 = $value;
                break;
            case 'resultatJ1':
                $this->resultatJ1 = $value;
                break;
            case 'resultatJ2':
                $this->resultatJ2 = $value;
                break;
        }
    }

    public function getGagnant()
    {
        if($this->resultatJ1 == 1)
        {
            return $this->joueur1;
        } else{
            return $this->joueur2;
        }
    }
    public function setResultat($res1, $res2) {
        $this->resultatJ1 = $res1;
        $this->resultatJ2 = $res2;
    }
}