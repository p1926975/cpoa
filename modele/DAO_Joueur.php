<?php
require_once('DTO_Joueur.php');

class DAO_Joueur {

	private $bdd;

	public function __construct()
	{
		try {
			$this->bdd = new PDO('mysql:host=127.0.0.1;dbname=gestionTennis;charset=UTF8', 'root', '');
		} catch(Exception $e) {
			die('Erreur: '.$e->getMessage());
		}
	}

	public function getById($i)
	{
		$sql = 'SELECT * FROM Joueur WHERE id=?';
		$req = $this->bdd->prepare($sql);
		$req->execute(array($i));
		$data = $req->fetch();
        $joueur = new Joueur((int)$data['id_n'],$data['nom_j'],$data['prenom_j'],$data['nationalite'],$data['date_naissance'],(int)$data['nbMatch_j'],(int)$data['nbVictoires'],$data['image']);
	    return $joueur;
	}

	public function getAll()
	{
		$tab = [];
		$sql = "SELECT * FROM joueur";
		$joueurs = $this->bdd->query($sql);
		foreach ($joueurs as $data) {
			$tab[] = new Joueur($data['id_j'],$data['nom_j'],$data['prenom_j'],$data['nationalite'],$data['date_naissance'],(int)$data['nbMatch_j'],(int)$data['nbVictoires'],$data['image']);
		}
		return $tab;
	}

	public function insert($n, $p, $nat, $dN, $im = "/null")
	{		
		$sql = "INSERT INTO joueur (nom_j, prenom_j, nationalite, date_naissance, image) VALUES (?, ?, ?, ?, ?)";
		$req = $this->bdd->prepare($sql);
		$req->execute(array($n, $p, $nat, $dN, $im));
	}

    /*public function addMatchJoue($i) {
        $sql = "UPDATE joueur SET nbMatch_j += 1 WHERE id= ?";
        $req = $this->bdd->prepare($sql);
        $req->execute(array($i));
    }*/

    /*public function addMatchGagne($i) {
        $sql = "UPDATE joueur SET nbVictoires += 1 WHERE id= ?";
        $req = $this->bdd->prepare($sql);
        $req->execute(array($i));
    }*/
}