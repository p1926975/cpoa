<?php
class Joueur {
    private $id_j;
    private $nom_j;
    private $prenom_j;
    private $nationalite;
    private $date_naissance;
    private $nbMatch_j;
    private $nbVictoires;
    private $image;
    //
    public function __construct($id=0, $n, $p, $nation, $dateN, $nbM, $nbV, $im)
    {
        $this->id_j = $id;
        $this->nom_j = $n;
        $this->prenom_j = $p;
        $this->nationalite = $nation;
        $this->date_naissance = $dateN;
        $this->nbMatch_j = $nbM;
        $this->nbVictoires = $nbV;
        $this->image = $im;
    }
    public function __get($name)
    {
        switch($name) {
            case 'id_j':
                return $this->id_j;
                break;
            case 'nom_j':
                return $this->nom_j;
                break;
            case 'prenom_j' :
                return $this->prenom_j;
            case 'nationalité' :
                return $this->nationalite;
                break;
            case 'date_naissance' :
                return $this->date_naissance;
                break;
            case 'nbMatch_j' :
                return $this->nbMatch_j;
                break ;
            case 'nbVictoires' :
                return $this->nbVictoires;
                break;
            case 'image' :
                return $this->image;
                break;
        }
    }

    public function getAge()
    {
        $date_n = new DateTime($this->date_naissance);
        $now = new DateTime();

        $age = $now->diff($date_n);

        return $age->format('%Y');
    }

    public function getWinRate()
    {
        if ($this->nbMAtch_j != 0) {
            $rate = 100 * $this->nbVictoires / $this->nbMAtch_j;
        } else {
            return "NAN";
        }
        return $rate;
    }

    public function addMatchJoue() {
        $this->nbMatch_j += 1;
    }

    public function addMatchGagne($i) {
        $this->nbVictoires += 1;
    }
}