<?php
require_once('DTO_Tour.php');

class DAO_Tour {

	private $bdd;

	public function __construct()
	{
		try {
			$this->bdd = new PDO('mysql:host=127.0.0.1;dbname=gestionTennis;charset=UTF8', 'root', '');
		} catch(Exception $e) {
			die('Erreur: '.$e->getMessage());
		}
	}

    public function getById($i)
	{
		$sql = 'SELECT * FROM Matchs WHERE id=?';
		$req = $this->bdd->prepare($sql);
		$rep = $req->execute([$i]);
		$data = $req->fetch();
		$tour = new Tour((int)['id'],['nom_tour'],['matchs']);
		return $tour;
	}

    public function getAll()
	{
		$tab = [];
		$sql = "SELECT * FROM Tour";
		$tour = $this->bdd->query($sql);
		foreach ($tour as $data) {
			$tab[] = new Tour((int)$data['id'], $data['nom_tour']);
		}
		return $tab;
	}

	public function addMatch($idmatch, $idtour)
	{
		$sql = "INSERT INTO Tournoi (id, id_m) VALUES (?, ?)";
		$req = $this->bdd->prepare($sql);
		$req->execute(array($idtour, $idmatch));
	}

	public function nbMatch($idTour)
	{
		$sql = 'SELECT count(*) FROM Match_Tour WHERE id = ?';
		$req = $this->bdd->prepare($sql);
		return $req->execute(array($idTour));
	}

	public function insert($nom, $match)
	{		
		$sql = "INSERT INTO tour (nom_tour, matchs) VALUES (?, ?)";
		$req = $this->bdd->prepare($sql);
		$req->execute(array($nom, $match));
	}

	public function chargementTab($matchs, $tours)
	{
		$tabM = [];
		$tabTour = [];
		$requeteM = "SELECT * FROM match_tour";

		$dataM = $this->bdd->query($requeteM);

		foreach ($tours as $tour) {
			foreach ($dataM as $datam) {
				foreach ($matchs as $match) {
					if ($datam['id_m'] == $match->id_m && $tour->id == $datam['id_t']) {
						$tabM[] = $match;
					}
				}
			}
			$tour->matchs = $tabM;
			$tabTour[] = $tour;
		}
		return $tabTour;
	}
}