<?php
require_once('DTO_Tournoi.php');
require_once('DTO_Joueur.php');
require_once('DTO_Tour.php');

class DAO_Tournoi
{

	private $bdd;

	public function __construct()
	{
		try {
			$this->bdd = new PDO('mysql:host=127.0.0.1;dbname=gestionTennis;charset=UTF8', 'root', '');
		} catch (Exception $e) {
			die('Erreur: ' . $e->getMessage());
		}
	}

	public function getById($i)
	{
		$sql = 'SELECT * FROM Tournoi WHERE id=?';
		$req = $this->bdd->prepare($sql);
		$req->execute(array($i));
		$data = $req->fetch();
		$tournoi = new Tournoi((int)$data['id_t'], $data['nom_t'], $data['date_debut'], $data['date_fin'], $data['nbMatch']);
		return $tournoi;
	}

	public function getAll()
	{
		$tab = [];
		$sql = "SELECT * FROM Tournoi";
		$joueurs = $this->bdd->query($sql);
		foreach ($joueurs as $data) {
			$tab[] = new Tournoi((int)$data['id_t'], $data['nom_t'], $data['date_debut'], $data['date_fin'], $data['nbMatch']);
		}
		return $tab;
	}

	public function insert($n, $dD, $dF, $nbM)
	{
		$sql = "INSERT INTO Tournoi (nom_t, date_debut, date_fin,nbMatch) VALUES (?, ?, ?,?)";
		$req = $this->bdd->prepare($sql);
		$req->execute(array($n, $dD, $dF, $nbM));
	}

	public function update($id, $n, $date_d, $date_f)
	{
		$sql = "UPDATE tournoi set nom_t=?, date_debut=?, date_fin=? where id_t=?";
		$requete = $this->bdd->prepare($sql);
		$requete->execute(array($n, $date_d, $date_f, $id));
	}

	public function ajoutJoueur($idJ, $idT)
	{
		$sql = "INSERT INTO Joueur_Tournoi (id_j, id_t) VALUES (?, ?)";
		$req = $this->bdd->prepare($sql);
		$req->execute(array($idJ, $idT));
	}

	public function supprimeJoueur($idJ, $idT)
	{
		$sql = "DELETE FROM joueur_Tournoi WHERE id_j=? and id_t=?";
		$req = $this->bdd->prepare($sql);
		$req->execute(array($idJ, $idT));
	}

	public function chargementTab($joueurs, $tours, $tournois)
	{
		$tabTourn = [];
		foreach ($tournois as $tournoi) {
			$requeteJ = "SELECT * from joueur_tournoi";
			$requeteT = "SELECT * FROM tour_tournoi";

			$dataJ = $this->bdd->query($requeteJ);
			$dataT = $this->bdd->query($requeteT);

			$tabJ = [];
			$tabT = [];
			foreach ($dataJ as $dataj) {
				foreach ($joueurs as $j) {
					if ($dataj['id_j'] == $j->id_j && $tournoi->id_t == $dataj['id_t']) {/*&& $tournoi->id_t == $dataj['id_t']*/
						$tabJ[] = $j;
					}
				}
			}
			foreach ($dataT as $datat) {
				foreach ($tours as $t) {
					if ($datat['id'] == $t->id && $tournoi->id_t == $datat['id_t']) {/*&& $tournoi->id_t == $datat['id_t']*/
						$tabT[] = $t;
					}
				}
			}
			$tournoi->joueurs = $tabJ;
			$tournoi->tours = $tabT;
			$tabTourn[] = $tournoi;
		}
		return $tabTourn;
	}
}
